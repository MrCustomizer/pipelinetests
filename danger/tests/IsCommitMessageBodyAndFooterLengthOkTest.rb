require_relative '../lib/commits'
require 'test/unit'
 
class IsCommitMessageBodyAndFooterLengthOkTest < Test::Unit::TestCase
 
    def test_shouldReturnTrue_WhenLineWith72CharactersIsProvided
        message = "123456789 123456789 123456789 123456789 123456789 "\
            "\n"\
            "123456789 123456789 123456789 123456789 123456789 123456789 123456789 12"\
            "\n"\
            "Issue: #123"
        assert_true(isCommitMessageBodyAndFooterLengthOk(message))
    end

    def test_shouldReturnTrue_WhenLineWith73CharactersIsProvided
            message = "123456789 123456789 123456789 123456789 123456789 "\
            "\n"\
            "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123"\
            "\n"\
            "Issue: #123"
        assert_false(isCommitMessageBodyAndFooterLengthOk(message))
    end

    def test_shouldReturnTrue_WhenOnlyHeaderWithMoreThan72CharactersIsProvided
        message = "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123"\
            "\n"\
            "123456789 123456789 123456789 123456789 123456789 123456789 123456789"\
            "\n"\
            "Issue: #123"
        assert_true(isCommitMessageBodyAndFooterLengthOk(message))
    end
end
