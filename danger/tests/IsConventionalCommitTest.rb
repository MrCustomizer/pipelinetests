require_relative '../lib/commits'
require 'test/unit'
 
class IsConventionalCommitTest < Test::Unit::TestCase
 
    def test_shouldReturnTrue_WhenShortMessageIsProvided
        message = "fix: test commit message\n"\
            "\n"\
            "Issue: #123"
        assert_true(isConventionalCommit(message))
    end

    def test_shouldReturnTrue_WhenShortMessageWithoutIssueColonIsProvided
        message = "fix: test commit message\n"\
            "\n"\
            "Issue #123"
        assert_true(isConventionalCommit(message))
    end

    def test_shouldReturnTrue_WhenLongMessageIsProvided
        message = "feat: test commit message\n"\
            "\n"\
            "body\n"\
            "\n"\
            "Issue: #123"
        assert_true(isConventionalCommit(message))
    end

    def test_shouldReturnTrue_WhenLongMessageWithoutIssueColonIsProvided
        message = "feat: test commit message\n"\
            "\n"\
            "body\n"\
            "\n"\
            "Issue #123"
        assert_true(isConventionalCommit(message))
    end

    def test_shouldReturnTrue_WhenLongMessageWithMultilineBodyIsProvided
        message = "docs: test commit message\n"\
            "\n"\
            "body line 1\n"\
            "body line 2\n"\
            "body line 3\n"\
            "\n"\
            "Issue: #123"
        assert_true(isConventionalCommit(message))
    end

    def test_shouldReturnFalse_WhenShortMessageFooterTooLongIsProvided
        message = "fix: test commit message\n"\
            "\n"\
            "Issue: #123\n"\
            "one line too much"
        assert_false(isConventionalCommit(message))
    end

    def test_shouldReturnFalse_WhenShortMessageFooterWithoutNewlineProvided
        message = "fix: test commit message\n"\
            "Issue: #123\n"
        assert_false(isConventionalCommit(message))
    end

    def test_shouldReturnFalse_WhenBodyWithoutNewlineIsProvided
        message = "fix: test commit message\n"\
            "body"
            "\n"\
            "Issue: #123\n"\
            "one line too much"
        assert_false(isConventionalCommit(message))
    end

    def test_shouldReturnFalse_WhenLongMessageFooterTooLongIsProvided
        message = "fix: test commit message\n"\
            "\n"\
            "body line 1\n"\
            "body line 2\n"\
            "\n"\
            "Issue: #123\n"\
            "one line too much"
        assert_false(isConventionalCommit(message))
    end

end
