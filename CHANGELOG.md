# [1.6.0](https://gitlab.com/MrCustomizer/pipelinetests/compare/v1.5.1...v1.6.0) (2021-05-08)


### Features

* new flutter-pi version ([a62c777](https://gitlab.com/MrCustomizer/pipelinetests/commit/a62c777d3c6baf4b095fed5cc698f38ea9e09cf2)), closes [#9](https://gitlab.com/MrCustomizer/pipelinetests/issues/9)
* update flutter-pi engine binaries ([3fa266a](https://gitlab.com/MrCustomizer/pipelinetests/commit/3fa266ae1adf5eab05c33216665b7c3e2532d169)), closes [#21](https://gitlab.com/MrCustomizer/pipelinetests/issues/21)

## [1.5.1](https://gitlab.com/MrCustomizer/pipelinetests/compare/v1.5.0...v1.5.1) (2020-12-16)


### Bug Fixes

* only one subdirectory ([e911c34](https://gitlab.com/MrCustomizer/pipelinetests/commit/e911c343b81bb26bb7aa409dd76d0c70f2e5a377)), closes [#9](https://gitlab.com/MrCustomizer/pipelinetests/issues/9)

# [1.5.0](https://gitlab.com/MrCustomizer/pipelinetests/compare/v1.4.0...v1.5.0) (2020-12-16)


### Features

* only one artifact file ([4bdae28](https://gitlab.com/MrCustomizer/pipelinetests/commit/4bdae2830c6641f1a2c82691be5c7f5dfdd93a63)), closes [#9](https://gitlab.com/MrCustomizer/pipelinetests/issues/9)

# [1.4.0](https://gitlab.com/MrCustomizer/pipelinetests/compare/v1.3.1...v1.4.0) (2020-12-16)


### Bug Fixes

* add missing dependency ([1cb2d77](https://gitlab.com/MrCustomizer/pipelinetests/commit/1cb2d7775bff04d36df768e8ad9fda46c10705c1)), closes [#9](https://gitlab.com/MrCustomizer/pipelinetests/issues/9)
* build order ([614d33c](https://gitlab.com/MrCustomizer/pipelinetests/commit/614d33cfd0506e66b478e1bff18a55d9c5a9e599)), closes [#9](https://gitlab.com/MrCustomizer/pipelinetests/issues/9)
* correct permission path ([86c66fb](https://gitlab.com/MrCustomizer/pipelinetests/commit/86c66fb3f0bc392f6880a0f4cddf110ec1a02669)), closes [#9](https://gitlab.com/MrCustomizer/pipelinetests/issues/9)


### Features

* add flutter-pi release build steps ([a676c59](https://gitlab.com/MrCustomizer/pipelinetests/commit/a676c59b9a2b7366245855242ca34212aab3df3c)), closes [#9](https://gitlab.com/MrCustomizer/pipelinetests/issues/9)
* add stages ([6181c75](https://gitlab.com/MrCustomizer/pipelinetests/commit/6181c75798af5d52758029d44ee8cfe3f200ca9f)), closes [#9](https://gitlab.com/MrCustomizer/pipelinetests/issues/9)
* only store flutter_assets path ([d1febca](https://gitlab.com/MrCustomizer/pipelinetests/commit/d1febca41e076cded61529759349b3a2295e3aac)), closes [#9](https://gitlab.com/MrCustomizer/pipelinetests/issues/9)
* rename job ([28cf93f](https://gitlab.com/MrCustomizer/pipelinetests/commit/28cf93fcb1094b2a0a12a807bf919bfaba004e0f)), closes [#9](https://gitlab.com/MrCustomizer/pipelinetests/issues/9)
* try out automatic release creation ([521c434](https://gitlab.com/MrCustomizer/pipelinetests/commit/521c4342fe81b6945840bc90723b6d7ccbe9baec)), closes [#9](https://gitlab.com/MrCustomizer/pipelinetests/issues/9)

## [1.3.1](https://gitlab.com/MrCustomizer/pipelinetests/compare/v1.3.0...v1.3.1) (2020-11-10)


### Bug Fixes

* Add changelog to release plugin ([32a6530](https://gitlab.com/MrCustomizer/pipelinetests/commit/32a6530ec90133b2a6eb3a6c99721137da5d1bb7)), closes [#19](https://gitlab.com/MrCustomizer/pipelinetests/issues/19)

## [1.2.1](https://gitlab.com/MrCustomizer/pipelinetests/compare/v1.2.0...v1.2.1) (2020-11-10)


### Bug Fixes

* something to fix ([85266fa](https://gitlab.com/MrCustomizer/pipelinetests/commit/85266fab066493199c6fe6da4928dbd12e3cb5a7)), closes [#18](https://gitlab.com/MrCustomizer/pipelinetests/issues/18)

# [1.2.0](https://gitlab.com/MrCustomizer/pipelinetests/compare/v1.1.0...v1.2.0) (2020-11-10)


### Bug Fixes

* Another pipeline problem ([66c179d](https://gitlab.com/MrCustomizer/pipelinetests/commit/66c179d94505d54df0c81b147719f98235a0a878)), closes [#15](https://gitlab.com/MrCustomizer/pipelinetests/issues/15)
* correct branch in release file ([480be87](https://gitlab.com/MrCustomizer/pipelinetests/commit/480be873b844db87187ec0592197f05b0a21f571)), closes [#16](https://gitlab.com/MrCustomizer/pipelinetests/issues/16)
* gitlab-ci syntax ([d84c9b8](https://gitlab.com/MrCustomizer/pipelinetests/commit/d84c9b84423664d1f28de865d69da14efdf02d4e)), closes [#15](https://gitlab.com/MrCustomizer/pipelinetests/issues/15)
* missing comma ([4415cbe](https://gitlab.com/MrCustomizer/pipelinetests/commit/4415cbe104e4046c1c7e63ae0694f03b94a3a788)), closes [#16](https://gitlab.com/MrCustomizer/pipelinetests/issues/16)
* Only run pipeline on release ([37832aa](https://gitlab.com/MrCustomizer/pipelinetests/commit/37832aa7ed0f7f0ef274e48de23a16ba161e1c24)), closes [#10](https://gitlab.com/MrCustomizer/pipelinetests/issues/10)
* Pipeline for release doesn't fail anymore ([240120c](https://gitlab.com/MrCustomizer/pipelinetests/commit/240120c8c3315bcf11b6f0da5d8a3e0844e15144)), closes [#15](https://gitlab.com/MrCustomizer/pipelinetests/issues/15)
* pipeline logic ([5fee0b2](https://gitlab.com/MrCustomizer/pipelinetests/commit/5fee0b29c7a312e1ca8e93b599367cdb09455111)), closes [#15](https://gitlab.com/MrCustomizer/pipelinetests/issues/15)


### Features

* another pipeline fix ([76f6194](https://gitlab.com/MrCustomizer/pipelinetests/commit/76f61941099fbb1421b04842b9c5798e2957f122)), closes [#15](https://gitlab.com/MrCustomizer/pipelinetests/issues/15)
* create releases on master branch again ([370b281](https://gitlab.com/MrCustomizer/pipelinetests/commit/370b281572fe480356e61c7d0cdca0fff6421cf1)), closes [#16](https://gitlab.com/MrCustomizer/pipelinetests/issues/16)
* Enforce referencing correct issue numbers ([647835c](https://gitlab.com/MrCustomizer/pipelinetests/commit/647835cb997efb07381fb658ca02acbc40022dc4)), closes [#11](https://gitlab.com/MrCustomizer/pipelinetests/issues/11)
* Only run SemRelease on release branch ([cc190eb](https://gitlab.com/MrCustomizer/pipelinetests/commit/cc190eb5ab3a4a0ef26caea845a0cbffb680a5c9)), closes [#10](https://gitlab.com/MrCustomizer/pipelinetests/issues/10)

# [1.1.0](https://gitlab.com/MrCustomizer/pipelinetests/compare/v1.0.0...v1.1.0) (2020-11-09)


### Features

* Don't enforce : in footer of commit message ([28bdc9c](https://gitlab.com/MrCustomizer/pipelinetests/commit/28bdc9ca8c64d54ee04ff401abc1c1d0cd8d747c)), closes [#6](https://gitlab.com/MrCustomizer/pipelinetests/issues/6)

# 1.0.0 (2020-11-09)


### Bug Fixes

* add git as dependency for deployment stage ([57da264](https://gitlab.com/MrCustomizer/pipelinetests/commit/57da264b3e2c4308ca7a89ff3005b8028f75fb9d)), closes [#5](https://gitlab.com/MrCustomizer/pipelinetests/issues/5)
* add git dependency ([1c176a3](https://gitlab.com/MrCustomizer/pipelinetests/commit/1c176a32144caf6212ff313365e87b4f10ed388d)), closes [#5](https://gitlab.com/MrCustomizer/pipelinetests/issues/5)
* add missing certificate dependency ([885f397](https://gitlab.com/MrCustomizer/pipelinetests/commit/885f397c68530c60e772fcc7fa408b378cb91f37)), closes [#5](https://gitlab.com/MrCustomizer/pipelinetests/issues/5)
* Change commit message format ([3f20aae](https://gitlab.com/MrCustomizer/pipelinetests/commit/3f20aae1c5649eacc496f7faf77d810d53d0c241)), closes [#3](https://gitlab.com/MrCustomizer/pipelinetests/issues/3)
* install missing dependency ([da61f5e](https://gitlab.com/MrCustomizer/pipelinetests/commit/da61f5ee524b62104e5cc2858ecb4f89b9ea5ab8)), closes [#5](https://gitlab.com/MrCustomizer/pipelinetests/issues/5)


### Features

* Add semantic release ([0742144](https://gitlab.com/MrCustomizer/pipelinetests/commit/074214455651a92142aff0060dde9a7ae05b4522)), closes [#5](https://gitlab.com/MrCustomizer/pipelinetests/issues/5)
* check commits in CI ([5963e92](https://gitlab.com/MrCustomizer/pipelinetests/commit/5963e922d2fbef473d5668355c835c0f4d9224be)), closes [#1](https://gitlab.com/MrCustomizer/pipelinetests/issues/1)
* check for line lengths ([2a2cf36](https://gitlab.com/MrCustomizer/pipelinetests/commit/2a2cf36cc5b6a9c5364eb7d7f89628ffd72f8af3)), closes [#4](https://gitlab.com/MrCustomizer/pipelinetests/issues/4)
* run tests in pipeline ([beaa5a5](https://gitlab.com/MrCustomizer/pipelinetests/commit/beaa5a5fa10ef05f13a4ff91b1572f05c86a4071)), closes [#2](https://gitlab.com/MrCustomizer/pipelinetests/issues/2)
